import telebot
from telebot import types
import random

bot = telebot.TeleBot('6262649854:AAHN-zCVHWcKCfWIz_vL3_Bm8UumsTP90ec')

comedy = ['Мачо и ботан 1/2/3', 'Третий лишний 1/2', 'Мальчишник в Вегасе (2009)', 'Одноклассники (2010)', 'Мы-Миллеры (2013)', 'Каникулы (2015)', 'Здравствуй, папа, Новый Год! 1/2', 'Зачетный препод 1/2/3', 'Типа копы (2014)', 'Шпион (2015)', 'Притворись моей женой (2011)', 'Впритык (2010)', 'В пролете (2008)', 'Шутки в сторону (2012)', 'Безумная свадьба (2014)', 'Superнянь (2014)', 'Кадры (2013)', 'Убойные каникулы (2010)', 'Почему он? (2016)', 'Нас приняли! (2006)', 'Невероятная жизнь Уолтера Митти (2013)']
cartoon = ['Ну, погоди! (1969-2017)', 'Зверополис (2016)', 'Рататуй (2007)', 'Шрэк', 'Король и лев', 'Тайна Коко (2017)', 'Душа (2020)', 'Головоломка (2015)', 'Гравити Фолз (2012-2016)', 'Корпорация монстров (2001)', 'Моанна (2016)', 'Мадагаскар (2005)', 'Мулан (1998)', 'Вверх (2009)', 'Ледниковый пориод', 'Аладдин (2019)', 'Красавица и чудовище (2017)', 'Как приручить дракона 1/2/3']
horror = ['Оно (2017)', 'Поезд в Пусан (2016)', 'Чужой (1979)', 'Прочь (2017)', 'Обитель зла (2002)', 'Заклятие (2013)', 'Мгла (2007)', 'Ключ от всех двере (2005)', 'Синистер (2012)', 'Астрал (2010)', 'Реинкарнация (2018)', 'Паранормальное явление 1/2/3/4/5/6', 'Бабадук (2014)', 'Спуск 1/2', 'Визит (2015)', 'Тихое место (2018)', 'Дьявол (2010)', 'Хижина в лесу (2011)', 'Звонок мертвецу (2018)']
fantasy = ['Автар (2009)', 'Начало (2010)', 'Интерстеллар (2014)', 'Дюна (2021)', 'Доктор Стрэндж (2016)', 'Матрица (1999)', 'Назад в будущее (1985)', 'Марсианин (2015)', 'Пятый элемент (1997)', 'Главный герой (2021)', 'Живая сталь (2011)', 'Пассажиры (2016)', 'Веном (2018)', 'Время (2011)', 'Господин Никто (2009)']
thriller = ['Бойцовский клуб (1999)', 'Остров проклятых (2009)', 'Области тьмы (2011)', 'Престиж (2006)', 'Исчезнувшая (2014)', 'Молчание ягнят (1990)', 'Сплит (2017)', 'Метод (2015-2021)', 'Невидимый гость (2016)', '127 часов (2010)', 'Точка кипения (2021)', 'Начало (2010)', 'Леон (1994)']
action = ['Гнев человеческий (2021)', 'Дэдпул (2016)', 'Законопослушный гражданин (2009)', 'Карты, деньги, два ствола (1998)', 'Никто (2021)', 'Крепкий орешек (1988)', 'Апгрейд (2018)', 'Неудержимые (2010)', 'Перевозчик (2002)', 'Джентельмены (2020)', 'Брат (1997)', 'Форсаж', 'Брат 2 (2000)']
drama = ['Титаник (1997)', 'Москва слезам не верит (1979)', 'Вечное сияние чистого разума (2004)', 'Отпуск по обмену (2006)', 'Стажер (2015)', 'В джазе только девушки (1959)', '500 дней лета (2009)', 'Реальная любовь (2003)', 'Аритмия (2017)', 'Ла-Ла Ленд (2016)', 'Она (2013)', '1+1 (2011)', 'Сумерки (2008)', 'В метре друг от друга (2019)', 'Маленькие женщины (2019)', 'До встречи с тобой (2016)', 'Мое прекрасное неасчастье (2023)', 'Терминал (2004)' ]
detectives = ['Достать ножи (2019)', 'Черный ящик (2020)', 'Девушка с татуировкой дракона (2011)', 'Фишер (2023)', 'Ветреная река (2017)', 'Дело Коллини (2019)', 'Убийство в Восточном экспрессе (2017)', 'Голем (2016)', 'Смерть на Ниле (2022)', 'Девушка в поезде (2016)', 'Патруль (2012)', 'Наркокурьер (2019)', 'Голос из камня (2017)']
family = ['Чебурашка (2022)', 'Счастье в конверте (2019)', 'Сердце чемпиона (2018)', 'Друг в океане (2022)', 'Приключения Паддингтона (2014)', '101 далматинец (1996)', 'Мы купили зоопарк (2011)', 'История игрушек (1995)', 'Зов предков (2019)', 'Приключения Реми (2018)', 'Призрак (2015)', 'Белль и Себастьян (2013)']

points = 0
words_win = ['Правильно!', 'Молодец!', 'Верно!', 'Отлично!', 'Так держать!', 'Ответ правильный!']
words_lose = ['Ответ неправильный', 'Неверно', 'К сожалению, твой ответ неправильный', 'Будь внимательнее!']

@bot.message_handler(content_types=['text'])
def get_text(message):
    if message.text == "/start":
        bot.send_message(message.chat.id, 'Привет! Не можешь выбрать, какой фильм посмотреть? Я помогу! Отправь в чат слово Привет ')
    elif message.text == "Привет" or message.text == "Продолжить":
        keyboard1 = types.InlineKeyboardMarkup()
        key_genre = types.InlineKeyboardButton(text='Выбрать фильм по жанру', callback_data='genre')
        keyboard1.add(key_genre)
        key_random = types.InlineKeyboardButton(text='Выбрать рандомный фильм', callback_data='random')
        keyboard1.add(key_random)
        key_quiz = types.InlineKeyboardButton(text='Пройти квиз на знание фильмов и всего того, что с ними связано', callback_data='quiz')
        keyboard1.add(key_quiz)
        key_list = types.InlineKeyboardButton(text='Посмотреть существующие подборки фильмов', callback_data='list')
        keyboard1.add(key_list)
        bot.send_message(message.chat.id, "Выбери то, что тебе нужно: ", reply_markup=keyboard1)
    else:
        bot.send_message(message.chat.id, "Я тебя не понимаю. Напиши Привет")

@bot.callback_query_handler(func=lambda call: True)
def callback_g(call):
    if call.data=="genre":
        keyboard2 = types.InlineKeyboardMarkup()
        key_drama = types.InlineKeyboardButton(text='Драма', callback_data='drama')
        keyboard2.add(key_drama)
        key_comedy = types.InlineKeyboardButton(text='Комедия', callback_data='comedy')
        keyboard2.add(key_comedy)
        key_cartoon = types.InlineKeyboardButton(text='Мультфильм', callback_data='cartoon')
        keyboard2.add(key_cartoon)
        key_horror = types.InlineKeyboardButton(text='Ужасы', callback_data='horror')
        keyboard2.add(key_horror)
        key_fantasy = types.InlineKeyboardButton(text='Фантастика', callback_data='fantasy')
        keyboard2.add(key_fantasy)
        key_action = types.InlineKeyboardButton(text='Боевик', callback_data='action')
        keyboard2.add(key_action)
        key_thriller = types.InlineKeyboardButton(text='Триллер', callback_data='thriller')
        keyboard2.add(key_thriller)
        key_family = types.InlineKeyboardButton(text='Семейные', callback_data='family')
        keyboard2.add(key_family)
        key_detectives = types.InlineKeyboardButton(text='Детектив', callback_data='detectives')
        keyboard2.add(key_detectives)
        bot.send_message(call.message.chat.id, "Выбери то, что тебе нужно: ", reply_markup=keyboard2)
    if call.data=="drama":
        msg1 = random.choice(drama) 
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg1}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="comedy":
        msg2 = {random.choice(comedy)}
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg2}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="cartoon":
        msg3 = random.choice(cartoon)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg3}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="horror":
        msg4 = random.choice(horror)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg4}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="fantasy":
        msg5 = random.choice(fantasy)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg5}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="action":
        msg6 = random.choice(action)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg6}')
        bot.send_message(call.message.chat.id, 'Для продолжения отпрваьте слово Продолжить')
    if call.data=="thriller":
        msg7 = random.choice(thriller)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg7}')
        bot.send_message(call.message.chat.id, 'Для продолжения отпрваьте слово Продолжить')
    if call.data=="family":
        msg8 = random.choice(family)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg8}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="detectives":
        msg9 = random.choice(detectives)
        bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg9}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="random":
        k = random.randint(1,9)
        if k==1:
            msg1 = random.choice(drama) 
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg1}')
        if k==2:
            msg2 = {random.choice(comedy)}
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg2}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==3:
            msg3 = random.choice(cartoon)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg3}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==4:
            msg4 = random.choice(horror)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg4}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==5:
            msg5 = random.choice(fantasy)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg5}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==6:
            msg6 = random.choice(action)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg6}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==7:
            msg7 = random.choice(thriller)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg7}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==8:
            msg8 = random.choice(family)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg8}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
        if k==9:
            msg9 = random.choice(detectives)
            bot.send_message(call.message.chat.id, f'Рекомендуем посмотреть следующий фильм: {msg9}')
            bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="list":
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'ТОП-10', callback_data='TOP10')
        item2 = types.InlineKeyboardButton(text = 'ТОП за последний год', callback_data='TOP')
        item3 = types.InlineKeyboardButton(text = 'Подборка самых страшных', callback_data='aaa')
        item4 = types.InlineKeyboardButton(text = 'Подборка, чтобы поплакать', callback_data='cry')
        markup.add(item1, item2, item3, item4)
        bot.send_message(call.message.chat.id, 'Выберите нужную подборку: ', reply_markup = markup)
    if call.data=="TOP10":
        bot.send_message(call.message.chat.id, 'ТОП-10: \n 1. Зеленая миля (1999) \n 2. Побег из Шоушенка (1994) \n 3. Форрест Гамп (1994) \n 4. Список Шиндлера (1993) \n 5. Тайна Коко (2017) \n 6. Властелин колец: Возвращение короля (2003) \n 7. Интерстеллар (2014) \n 8. Бойцовский клуб (1999) n\ 9. Криминальное чтиво (1994) \n 10. Унесённые призраками (2001)')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="TOP":
        bot.send_message(call.message.chat.id, 'ТОП-10 за последний год: \n 1. Вышка (2022) \n 2. Мажор в Сочи (2022) \n 3. Операция «Фортуна»: Искусство побеждать (2022) \n 4. Чебурашка (2022) \n 5. Три тысячи лет желаний (2022) \n 6. Треугольник печали (2022) \n 7. Крушение (2022) \n 8. Гарри Поттер 20 лет спустя: Возвращение в Хогвартс (2022) n\ 9. Мистер Нокаут (2022) \n 10. Мира (2022)')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=='aaa':
        bot.send_message(call.message.chat.id, 'Самые страшные фильмы: : \n 1. Заклятие (2013, 2016) \n 2. Зеракала (2008) \n 3. Синистер (2012) \n 4. Сайлент хил (2006) \n 5. Астрал (2011) \n 6. Проклятие (2002) \n 7. В пасти безумия (1995) \n 8. Мертавая тишина (2007) \n 9. Шесть демонов Эмили Роуз (2005) \n 10. Паранормальное явление (2007-2015)')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="cry":
        bot.send_message(call.message.chat.id, 'Подборка, если хотите поплакать: : \n 1. Дорогой Джон (2010) \n 2. Привидение (1990) \n 3. В метре друг от друга» (2019) \n 4. Неспящие в Сиэттле (1993) \n 5. Спеши любить (2002) \n 6. Последняя любовь на Земле (2010) \n 7. Титаник (1997) \n 8. Дневник памяти (2004) \n 9. Враг мой (1985) \n 10. Хатико: Самый верный друг (2008)')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    if call.data=="quiz":
        global points
        global words_lose
        global words_win
        points = 0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Леонардо Ди Каприо', callback_data='l')
        item2 = types.InlineKeyboardButton(text = 'Брэд Питт', callback_data='b')
        item3 = types.InlineKeyboardButton(text = 'Вин Дизель', callback_data='v')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '1) Кто исполнил главную мужскую роль в фильме Титаник?', reply_markup = markup)
    if call.data == 'l':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '4', callback_data='4')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='8')
        item3 = types.InlineKeyboardButton(text = '10', callback_data='10')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '2) Какая последняя часть Форсажа была выпущена?', reply_markup = markup)
    elif call.data == 'b':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Леонардо Ди Каприо')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '4', callback_data='4')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='8')
        item3 = types.InlineKeyboardButton(text = '10', callback_data='10')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '2) Какая последняя часть Форсажа была выпущена?', reply_markup = markup)
    elif call.data == 'v':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Леонардо Ди Каприо')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '4', callback_data='4')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='8')
        item3 = types.InlineKeyboardButton(text = '10', callback_data='10')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '2) Какая последняя часть Форсажа была выпущена?', reply_markup = markup)
    if call.data == '10':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Его отец - инопланетянин', callback_data='his')
        item2 = types.InlineKeyboardButton(text = 'Весь мир следил за ним', callback_data='all')
        item3 = types.InlineKeyboardButton(text = 'Он - избранный, который спасёт весь мир', callback_data='he')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '3) Какой секрет раскрыл главный герой фильма «Шоу Трумэна»?', reply_markup = markup)
    elif call.data == '4':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 10')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Его отец - инопланетянин', callback_data='his')
        item2 = types.InlineKeyboardButton(text = 'Весь мир следил за ним', callback_data='all')
        item3 = types.InlineKeyboardButton(text = 'Он - избранный, который спасёт весь мир', callback_data='he')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '3) Какой секрет раскрыл главный герой фильма «Шоу Трумэна»?', reply_markup = markup)
    elif call.data == '8':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 10')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Его отец - инопланетянин', callback_data='his')
        item2 = types.InlineKeyboardButton(text = 'Весь мир следил за ним', callback_data='all')
        item3 = types.InlineKeyboardButton(text = 'Он - избранный, который спасёт весь мир', callback_data='he')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '3) Какой секрет раскрыл главный герой фильма «Шоу Трумэна»?', reply_markup = markup)
    if call.data == 'all':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Гарри и Марвин', callback_data='harry')
        item2 = types.InlineKeyboardButton(text = 'Джон и Брэд', callback_data='john')
        item3 = types.InlineKeyboardButton(text = 'Питер и Стэнли', callback_data='piter')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '4) Как звали двух главных злодеев в фильме «Один дома»?', reply_markup = markup)
    elif call.data == 'his':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Весь мир следил за ним')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Гарри и Марвин', callback_data='harry')
        item2 = types.InlineKeyboardButton(text = 'Джон и Брэд', callback_data='john')
        item3 = types.InlineKeyboardButton(text = 'Питер и Стэнли', callback_data='piter')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '4) Как звали двух главных злодеев в фильме «Один дома»?', reply_markup = markup)
    elif call.data == 'he':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Весь мир следил за ним')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Гарри и Марвин', callback_data='harry')
        item2 = types.InlineKeyboardButton(text = 'Джон и Брэд', callback_data='john')
        item3 = types.InlineKeyboardButton(text = 'Питер и Стэнли', callback_data='piter')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '4) Как звали двух главных злодеев в фильме «Один дома»?', reply_markup = markup)
    if call.data == 'harry':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '111', callback_data='111')
        item2 = types.InlineKeyboardButton(text = '110', callback_data='110')
        item3 = types.InlineKeyboardButton(text = '101', callback_data='101')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '5) Сколько собак в знаменитом фильме про далматинцев?', reply_markup = markup)
    elif call.data == 'john':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Гарри и Марвин')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '111', callback_data='111')
        item2 = types.InlineKeyboardButton(text = '110', callback_data='110')
        item3 = types.InlineKeyboardButton(text = '101', callback_data='101')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '5) Сколько собак в знаменитом фильме про далматинцев?', reply_markup = markup)
    elif call.data == 'piter':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Гарри и Марвин')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '111', callback_data='111')
        item2 = types.InlineKeyboardButton(text = '110', callback_data='110')
        item3 = types.InlineKeyboardButton(text = '101', callback_data='101')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '5) Сколько собак в знаменитом фильме про далматинцев?', reply_markup = markup)
    if call.data == '101':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '3', callback_data='three')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='eight')
        item3 = types.InlineKeyboardButton(text = '9', callback_data='nine')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '6) Сколько есть фильмов "Пила?', reply_markup = markup)
    elif call.data == '110':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 101')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '3', callback_data='three')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='eight')
        item3 = types.InlineKeyboardButton(text = '9', callback_data='nine')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '6) Сколько есть фильмов "Пила?', reply_markup = markup)
    elif call.data == '111':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 101')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '3', callback_data='three')
        item2 = types.InlineKeyboardButton(text = '8', callback_data='eight')
        item3 = types.InlineKeyboardButton(text = '9', callback_data='nine')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '6) Сколько есть фильмов "Пила?', reply_markup = markup)
    if call.data == 'nine':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'В Ирландии', callback_data='ireland')
        item2 = types.InlineKeyboardButton(text = 'В В Новой Зеландии', callback_data='newzeland')
        item3 = types.InlineKeyboardButton(text = 'В Исландии', callback_data='iceland')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '7) Где снимали трилогию «Властелин колец»?', reply_markup = markup)
    elif call.data == 'three':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 9')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'В Ирландии', callback_data='ireland')
        item2 = types.InlineKeyboardButton(text = 'В В Новой Зеландии', callback_data='newzeland')
        item3 = types.InlineKeyboardButton(text = 'В Исландии', callback_data='iceland')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '7) Где снимали трилогию «Властелин колец»?', reply_markup = markup)   
    elif call.data == 'eight':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 9')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'В Ирландии', callback_data='ireland')
        item2 = types.InlineKeyboardButton(text = 'В Новой Зеландии', callback_data='newzeland')
        item3 = types.InlineKeyboardButton(text = 'В Исландии', callback_data='iceland')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '7) Где снимали трилогию «Властелин колец»?', reply_markup = markup)
    if call.data == 'newzeland':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Флори', callback_data='flori')
        item2 = types.InlineKeyboardButton(text = 'Гримм', callback_data='grim')
        item3 = types.InlineKeyboardButton(text = 'Эренделл', callback_data='erenden')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '8) Как называется вымышленная страна, где происходит действие в «Холодном сердце»?', reply_markup = markup) 
    elif call.data == 'iceland':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: В Новой Зеландии')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Флори', callback_data='flori')
        item2 = types.InlineKeyboardButton(text = 'Гримм', callback_data='grim')
        item3 = types.InlineKeyboardButton(text = 'Эренделл', callback_data='erenden')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '8) Как называется вымышленная страна, где происходит действие в «Холодном сердце»?', reply_markup = markup)  
    elif call.data == 'ireland':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: В Новой Зеландии')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Флори', callback_data='flori')
        item2 = types.InlineKeyboardButton(text = 'Гримм', callback_data='grim')
        item3 = types.InlineKeyboardButton(text = 'Эренделл', callback_data='erenden')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '8) Как называется вымышленная страна, где происходит действие в «Холодном сердце»?', reply_markup = markup)  
    if call.data == 'erenden':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Уэс Андерсон', callback_data='yes')
        item2 = types.InlineKeyboardButton(text = 'Оливер Стоун', callback_data='oliver')
        item3 = types.InlineKeyboardButton(text = 'Джонатан Демме', callback_data='dj')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '9) Кто снял «Молчание ягнят»?', reply_markup = markup)   
    elif call.data == 'flori':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Эренделл')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Уэс Андерсон', callback_data='yes')
        item2 = types.InlineKeyboardButton(text = 'Оливер Стоун', callback_data='oliver')
        item3 = types.InlineKeyboardButton(text = 'Джонатан Демме', callback_data='dj')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '9) Кто снял «Молчание ягнят»?', reply_markup = markup)     
    elif call.data == 'grim':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Эренделл')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = 'Уэс Андерсон', callback_data='yes')
        item2 = types.InlineKeyboardButton(text = 'Оливер Стоун', callback_data='oliver')
        item3 = types.InlineKeyboardButton(text = 'Джонатан Демме', callback_data='dj')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '9) Кто снял «Молчание ягнят»?', reply_markup = markup)
    if call.data == 'dj':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '7 дней', callback_data='7d')
        item2 = types.InlineKeyboardButton(text = '5 дней', callback_data='5d')
        item3 = types.InlineKeyboardButton(text = '1 день', callback_data='1d')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '10) Сколько оставалось жить тем, кто смотрел проклятую видеокассету в «Звонке»?', reply_markup = markup)
    elif call.data == 'yes':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Джонатан Демме')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '7 дней', callback_data='7d')
        item2 = types.InlineKeyboardButton(text = '5 дней', callback_data='5d')
        item3 = types.InlineKeyboardButton(text = '1 день', callback_data='1d')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '10) Сколько оставалось жить тем, кто смотрел проклятую видеокассету в «Звонке»?', reply_markup = markup)
    elif call.data == 'oliver':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: Джонатан Демме')
        points+=0
        markup = types.InlineKeyboardMarkup()
        item1 = types.InlineKeyboardButton(text = '7 дней', callback_data='7d')
        item2 = types.InlineKeyboardButton(text = '5 дней', callback_data='5d')
        item3 = types.InlineKeyboardButton(text = '1 день', callback_data='1d')
        markup.add(item1, item2, item3)
        bot.send_message(call.message.chat.id, '10) Сколько оставалось жить тем, кто смотрел проклятую видеокассету в «Звонке»?', reply_markup = markup)
    if call.data == '7d':
        bot.send_message(call.message.chat.id, random.choice(words_win))
        points+=1
        bot.send_message(call.message.chat.id, f'Вы набрали: {points}')    
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')   
    elif call.data == '5d':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 9')
        points+=0   
        bot.send_message(call.message.chat.id, f'Вы набрали: {points}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
    elif call.data == '1d':
        bot.send_message(call.message.chat.id, random.choice(words_lose))
        bot.send_message(call.message.chat.id, 'Ответ: 9')
        points+=0
        bot.send_message(call.message.chat.id, f'Вы набрали: {points}')
        bot.send_message(call.message.chat.id, 'Для продолжения отправьте слово Продолжить')
bot.polling(none_stop=True)