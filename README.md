Проект реализован Надежкиной Е.А. 
студентом группы БСБО-10-21

Отчет по проекту «Телеграмм-бот FILMS SEARCH»

ОПИСАНИЕ: Данный телеграмм-бот предназначен для подбора фильмов. Необходим в тех случаях, когда пользователь испытывает трудности в выборе фильма.

РЕАЛИЗОВАННЫЙ ФУНКЦИОНАЛ: 
•	Подбор фильма по желаемому жанру
•	Рандомайзер фильма
•	Готовые подборки фильмов
•	«Интерактив» в виде квиза на знание фильмов/актеров/режиссеров

Код написан на языке Python с применением специальной библиотеки telebot (pyTelegramBotAPI).

Наглядное изображение бота можно увидеть на рисунке 1.

 ![alt text](result.png)




